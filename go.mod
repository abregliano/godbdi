module gitlab.com/abregliano/godbdi

go 1.21.9

require github.com/asdine/storm/v3 v3.2.1

require (
	go.etcd.io/bbolt v1.3.4 // indirect
	golang.org/x/sys v0.0.0-20200202164722-d101bd2416d5 // indirect
)

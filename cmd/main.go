package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"gitlab.com/abregliano/godbdi/dbstorm"
)

// type User struct {
//	ID    int
//	Group string
//	Email string
//	Name  string
//	Age   int
//}

type User = dbstorm.User

type Storage interface {
	Save(user User) (int, error)
	Delete(id int) error
	Get(id int) User
}

func main() {
	var s = dbstorm.New()
	defer s.Close()
	var u = User{
		Name:  "Paolo",
		Group: "Piolo",
		// ID:   1,
	}
	i, err := s.Save(u)
	if err != nil {
		log.Println(err)
	}
	fmt.Println(i)
	u, err = s.GetID(i)
	if err != nil {
		log.Println(err)
	}
	fmt.Println(u)
	s.Delete(i)
	idd, _ := strconv.Atoi(os.Args[1])
	s.Delete(idd)
	u, err = s.GetID(idd)
	if err != nil {
		log.Println(err)
	}
	fmt.Println(u)
}

package dbstorm

import (
	"github.com/asdine/storm/v3"
)

type DBStorm struct {
	DB *storm.DB
}

func (dbstorm *DBStorm) Close() {
	dbstorm.DB.Close()
}

func (dbstorm *DBStorm) Delete(id int) error {
	var u User
	dbstorm.DB.One("ID", id, &u)
	return dbstorm.DB.Drop(&u)
}

func (dbstorm *DBStorm) Save(user User) (int, error) {
	err := dbstorm.DB.Save(&user)
	return user.ID, err
}

func New() *DBStorm {
	var dbstorm = new(DBStorm)
	db, err := storm.Open("my.db")
	if err != nil {
		panic(err)
	}
	dbstorm.DB = db
	return dbstorm
}
